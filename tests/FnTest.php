<?php

use Illuminate\Foundation\Testing\CrawlerTrait;
use Illuminate\Foundation\Testing\WithoutMiddleware;

/**
 * FnTest class
 *
 * a) All details from .config should be active on Futurenet Backend
 * b) start command "phpunit" from "../app/FnTranslation"
 *
 * @package FnTest
 *
 * @since 1.0.1
 */
class FnTest extends TestCase {

	private $futurenetDomain = '';
	private $token = '';

	use WithoutMiddleware;
	use CrawlerTrait;

	public function __construct() {
		parent::__construct();

		// .config array
		$this->token = parse_ini_file(__DIR__ . '/../.config')['TOKEN'];
		$this->futurenetDomain = parse_ini_file(__DIR__ . '/../.config')['FUTURENET_URL'];

		$this->baseUrl = 'http://translate';

		// FnTranslationManager
		$this->manager = $manager;
	}

	public function testFn() {
		// test 1
		$this->getLanguages();
		// test 2
		$this->getExportArray();
		// test 3
		$this->getImportArray();
	}

	// Get all available language Codes from Fn API and - check Json structure
	public function getLanguages() {
		// local Request
		$request = $this->json('GET', '/fntranslation/available-langs');
		// response
		$content = $request->response->getOriginalContent();
		// JSON to Php Array
		$respArray = (array) json_decode($content);
		// Array has key
		$this->assertArrayHasKey('records', $respArray);
		$this->assertEquals($respArray['records'][0]->code, 'ua');
	}

	// Prepear structure for export JSON
	public function getExportArray() {
		// local Request
		$request = $this->get('/fntranslation/export-array');
		// response
		$respArray = $request->response->getOriginalContent();

		// Array has key
		$this->assertTrue(
			is_string($respArray[0]['var_name']),
			'"var_name" - Contain string'
		);
		$this->assertTrue(
			is_string($respArray[0]['desciprtion']),
			'"desciprtion" - Contain string'
		);
		$this->assertTrue(
			is_numeric($respArray[0]['project_id']),
			'"project_id" - Contain Numeric'
		);
	}

	// Check JSON for import
	public function getImportArray() {
		// local Request
		$request = $this->json('GET', '/fntranslation/import-array');
		// response
		$content = $request->response->getOriginalContent();
		// JSON to Php Array
		$respArray = (array) json_decode($content);

		// response Array has key
		$this->assertArrayHasKey('lang_structure', $respArray);
		// have property
		$this->assertTrue(
			is_string($respArray['lang_structure'][0]->path),
			'"path" - Object property contain string'
		);
		$this->assertTrue(
			is_string($respArray['lang_structure'][0]->var),
			'"var" - Object property contain string'
		);
		$this->assertTrue(
			is_string($respArray['lang_structure'][0]->value),
			'"value" - Object property contain string'
		);
	}

}
