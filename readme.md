## Laravel 5 "FnTranslation Plugin"


### Installation 

1. Download "FnTranslation" pluging
2. Place "FnTranslation" and its files in the "app" directory
3. In "config/app.php" at 'providers' Array "[ here ]", add "App\FnTranslation\FnTranslationServiceProvider::class," before the end "]" of the Array

### Home page

Use `http://yours-site.com/fntranslation` for home page. To prevent accessing this page for any (guest) user, please uncomment code at "FnTranslation/Controllers/FnTranslationController.php" in "public function index()", so only authorized users can see it

### Description

Update your Laravel website with new translations from your Futurenet BackOffice. This plugin is synchronizing your Laravel website text/translations with Futurenet BackOffice.

Following Laravel localization standards, every "view" (php) file contain: static text, variable or trans('key'), which can be translated to other languages using Ex.: <span><?php trans('text/key')?></span>

If site never was localized or was partialy translated, site administrator can include all text (or relative "key" name) in "../views/.." (PHP files) in "trans(text/key)" (or other Laravel) function to apply it for sit localization (translations), then from your Futurenet BackOffice export it to Futurenet for translations, and after site-text is translated, import it back to your Laravel website

If your site already using trans('key') style, where "key" coresponding to its php Array (at "/resources/lang/en/.." files) as: ['aboutus' => 'About Us']

Note: Your old language files in "./resources/lang" will be exchanged (and copied ex. "lang_2017-04-14_04-58-34") with new ones to your Laravel project

### Developer Docs

http://fnukraine.pp.ua/laravel-plugin
