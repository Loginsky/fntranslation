<?php

namespace App\FnTranslation;

/**
 * ServiceProvider
 *
 * @package App\FnTranslation
 */

use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class FnTranslationServiceProvider extends ServiceProvider {

	protected $namespace = 'App\FnTranslation\Controllers';
	protected $files;

	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot(Router $router) {
		parent::boot($router);

		if (is_dir(app_path() . '/FnTranslation/')) {
			// Allow routes to be cached
			if (!$this->app->routesAreCached()) {
				include_once app_path() . '/FnTranslation/routes.php';
			}

			// Views
			$views = app_path() . '/FnTranslation/Views';
			if ($this->files->isDirectory($views)) {
				$this->loadViewsFrom($views, 'FnTranslation');
			}
		}
	}

	public function map(Router $router) {

	}

	public function register() {
		$this->files = new Filesystem();
		require_once app_path('FnTranslation/helpers/fnTrans.php');
	}
}