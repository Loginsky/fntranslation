<?php namespace App\FnTranslation;

use Session;

/**
 * Class FnTranslationManager - responsible for plugin logic
 */

class FnTranslationBase {

	/**
	 * Get project languages [key => codes]
	 */
	public static function getProjectLangs($langCodeLangth = 'long') {
		$a = include_once app_path() . '/FnTranslation/storage/project-langs';
		if (empty($a)) {
			return [];
		}
		if ($langCodeLangth == 'short') {
			$newA = [];
			foreach ($a as $langKey => $langCode) {
				$newA[$langKey] = substr($langCode, 0, 2);
			}
			return $newA;
		} else {
			return $a;
		}
	}

	/**
	 * Make an http POST request and return the response content and headers
	 * @param string $url    url of the requested script
	 * @param array $data    hash array of request variables
	 * @return returns a hash array with response content and headers in the following form:
	 * array ('content'=>'<html></html>'
	 * , 'headers'=>array ('HTTP/1.1 200 OK', 'Connection: close', ...)
	 * )
	 */
	public static function httpRequest($url, $method = 'POST', $data = []) {
		$data_url = http_build_query($data);

		$aContent = array('method' => $method,
			'header' => 'Content-type: application/x-www-form-urlencoded',
			'content' => $data_url,
		);

		return file_get_contents($url, false, stream_context_create(array('http' => $aContent)));
	}

	// Send text to Futurenet for translations
	public static function sendNewFnTrans($aTranslation) {
		// new text, send to api for translations
		$api = '/put/project/translation';
		$project_id = parse_ini_file(app_path() . '/FnTranslation/.config')['PROJECT_ID'];
		$token = parse_ini_file(app_path() . '/FnTranslation/.config')['TOKEN'];
		$ip = parse_ini_file(app_path() . '/FnTranslation/.config')['FUTURENET_IP'];
		$fnUrl = parse_ini_file(app_path() . '/FnTranslation/.config')['FUTURENET_URL'];
		$aProjectLangs = static::getProjectLangs();
		// get current language code
		$getLangCode = '';
		if ($aProjectLangs) {
			foreach ($aProjectLangs as $langCode) {
				if (substr($langCode, 0, 2) == Session::get('locale')) {
					$getLangCode = $langCode;
				}
			}
		}
		$aPostVars = [
			'token' => $token,
			'project_id' => $project_id,
			'lang' => $getLangCode,
			'translation' => $aTranslation,
		];

		// send data to Futurenet
		self::httpRequest($fnUrl . $api, 'POST', $aPostVars);
	}
}
