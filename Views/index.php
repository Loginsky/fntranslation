<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>FnTrunslation Plugin</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script
  src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
  integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
  crossorigin="anonymous"></script>
</head>
<body class="container">

<h2>Wellcome to FnTranslation Plugin</h2>

<div class="panel panel-default">
  <div class="panel-heading">Visit your Futurenet BackOffice</div>
  <div class="panel-body">
    <div class="">
        <ul class="list-inline">
          <li><a target="_blank" href="http://fnukraine.pp.ua/#!/registration">BackOffice</a></li>
          <li><a target="_blank" href="#">API</a></li>
          <li><a target="_blank" href="#">Documentation</a></li>
          <li><a target="_blank" href="#">Tutorials</a></li>
          <li><a target="_blank" href="#">About us</a></li>
        </ul>
      </div>

    <p>Update your Laravel website with new translations from your Futurenet BackOffice. This plugin is synchronizing your Laravel website text/translations with Futurenet BackOffice</p>
  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">Get Your website text in Laravel <b>trans(text/key)</b> function for future translations</div>
  <div class="panel-body">

    <p>Following Laravel localization <a target="_blank" href="https://laravel.com/docs/5.1/localization">standards</a>, every "view" (php) file contain: <i>static text, variable or trans('key')</i>, which can be translated to other languages using Ex.: <i>&lt;span&gt;trans('text/key')&lt;/span&gt;</i></p>

    <p>If site never was localized or was partialy translated, site administrator can include all text (or relative "key" name) in "../views/.." (PHP files) in "trans(text/key)" (or other Laravel) function to apply it for sit localization (translations), then from your Futurenet BackOffice export it to Futurenet for translations, and after site-text is translated, import it back to your Laravel website</p>

    <p>If your site already using trans('key') style, where "key" coresponding to its php Array (<i>at "/resources/lang/en/.." files</i>) as: ['aboutus' => 'About Us']</p>

    <div class="alert alert-danger" role="alert">
      Your old language files in "./resources/lang" will be exchanged (and copied ex. "lang_2017-04-14_04-58-34") with new ones to your Laravel project
    </div>

  </div>
</div>

<div class="panel panel-default">
  <div class="panel-heading">Switch site language</div>
  <div class="panel-body">
    <p>Default language: <b>EN</b></p>
    <p>FnTrunslation plugin can change site language, keeping "language" sate in session, not url</p>
    <p>Available languages: <b>En, Fr</b></p>
      &lt;div style="block:inline;"&gt;<br>
        &nbsp;&nbsp;&nbsp; &lt;a href="/fntranslation/language/en"&gt;En&lt;/a&gt;<br>
        &nbsp;&nbsp;&nbsp; &lt;a href="/fntranslation/language/fr">Fr&lt;/a&gt;<br>
        &lt;/div&gt;
        <p><b>Example: </b></p>
          <div class="navbar-logo" style="left:100px; block:inline;">
              <a href="/fntranslation/language/en">En</a>
              <a href="/fntranslation/language/fr">Fr</a>
          </div>
  </div>
</div>

</body>
</html>
