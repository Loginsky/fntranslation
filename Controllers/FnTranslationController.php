<?php namespace App\FnTranslation\Controllers;

use App;
use App\FnTranslation\FnTranslationManager;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;

/**
 * Class FnTranslationController - responsible for plugin api
 */

class FnTranslationController extends Controller {

	private $token = '';
	private $futurenetDomain = '';
	private $project_id = '';
	private $default_lang = '';

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;
	protected $manager;
	protected $request;

	// Constructor
	public function __construct(FnTranslationManager $manager, Guard $auth, Request $request) {

		// .config array
		$this->token = parse_ini_file(app_path() . '/FnTranslation/.config')['TOKEN'];
		$this->futurenetDomain = parse_ini_file(app_path() . '/FnTranslation/.config')['FUTURENET_URL'];
		$this->project_id = parse_ini_file(app_path() . '/FnTranslation/.config')['PROJECT_ID'];
		$this->default_lang = parse_ini_file(app_path() . '/FnTranslation/.config')['DEFAULT_LANG'];

		// Request
		$this->request = $request;
		// authenticate super admin user
		$this->auth = $auth;

		// auth on
		$aAuthUrls = [
			'/fntranslation',
		];
		$urlPath = '/' . implode('/', $this->request->segments());
		$aUrlSegments = $this->request->segments();

		// FnTranslationManager
		$this->manager = $manager;

		// Check IP to allow request to plugin
		if (
			$this->manager->checkRequestIP($request) === false &&
			strpos($urlPath, 'fntranslation/language') === false &&
			($aUrlSegments && $aUrlSegments[sizeof($aUrlSegments) - 1] !== 'fntranslation')
		) {
			print "Request not allowed";
			exit;
		}

	}

	// Used for PhpUnit checking "export" structure
	public function exportLangsArray() {
		return $this->manager->exportLangs();
	}

	// Used for PhpUnit checking "import" structure
	public function importLangsArray() {
		$api = '/plugin_export_langs';
		$aPostVars = [
			'token' => $this->token,
			'default_lang' => $this->default_lang,
			'project' => $this->project_id,
		];
		return $this->manager::httpRequest($this->futurenetDomain . $api, 'POST', $aPostVars);
	}

	/*
		* Export data from App to Futurenet Server:
			API Prams (POST)
				[
					'token',
					'lang_structure' => JSON,
					'project',
				]
				return (JSON) [
					'status' => 'ok'
				]
		*
		* @return json
	*/
	public function pluginExportLangs() {
		$api = '/put/project/lang_var';

		$aExport = $this->manager->exportLangs();

		$aPostVars = [
			'token' => $this->token,
			'lang_structure' => json_encode($aExport),
			'project' => $this->project_id,
		];
		// request
		if ($this->manager::httpRequest($this->futurenetDomain . $api, 'POST', $aPostVars)) {
			return ['status' => 'ok'];
		} else {
			return ['status' => 'error'];
		}
	}

	/**
	 * Import from Futurenet Json (into files):
	 * [
	 *	{
	 *		"var":"rolesname",
	 *		"value":"Roles!",
	 *		"path":"en\/admin\/admin.php",
	 *		"language_id":1,
	 *	},{
	 *		"var":"stepsname",
	 *		"value":"Project steps!",
	 *		"path":"en\/admin\/admin.php",
	 *		"language_id":1,
	 *	}
	 * ]
	 *  @return Array ['status' => 'ok']
	 */
	public function pluginImportLangs() {
		$api = '/plugin_export_langs';
		$aPostVars = [
			'token' => $this->token,
			'default_lang' => $this->default_lang,
			'project' => $this->project_id,
		];

		$jsonImportLangs = $this->manager::httpRequest($this->futurenetDomain . $api, 'POST', $aPostVars);

		$oImportLangs = json_decode($jsonImportLangs);

		if (isset($oImportLangs->lang_structure) && $oImportLangs->lang_structure) {
			// add new translations in files
			$numberOfLangFiles = $this->manager->importLangs($oImportLangs);

			return [
				'status' => 'ok',
				'numberOfLangFiles' => $numberOfLangFiles,
			];
		} else {
			return [
				'status' => 'error',
				'message' => "'lang_structure' - is empty or not exists",
			];
		}

	}

	/**
	 * Plugin home page.
	 *
	 * @return View
	 */
	public function index() {
		// $admin = @$this->auth->user()->admin;
		// // check if admin
		// if (!$this->auth->check() && !$admin) {
		// 	$this->auth->logout();
		// 	return redirect('auth/login');
		// }

		return view("FnTranslation::index");
	}

	/**
	 * Extra feature:
	 * 	Change language globaly for all user website (no auth)
	 *	"language" is kept in Session
	 *
	 * This function is valid Plugin and Laravel website functionality
	 * NOTE: Also could be used as an example
	 * Followed - http://stackoverflow.com/questions/19249159/best-practice-multi-language-website
	 *
	 * @return Redirect (to prev page)
	 */
	public function language($lang = '') {
		if (!$lang) {
			$lang = $this->manager->getLangFromUrlQuery($this->request);
		}
		App::setLocale($lang);
		// use Session::get('language') in views
		$this->request->session()->put('locale', $lang);

		return redirect()->back();
	}

	/**
	 * Helper, get all "trans(text)" (or simular Laravel) functions
	 * for translation in view (php) files
	 *
	 * @return Array
	 */
	public function getAllTrans() {
		// update project languages in the plugin "storage" folder
		$aProjectLanguages = $this->manager->updateProjectLangs();

		// Find all trans() functions in view files
		$aResponse = $this->manager->findTransFunctions();

		return $aResponse;
	}

	/**
	 * Update all view files where "trans(text/key/$text)" is used (Ex.: with DB) have
	 * additional "fnTrans(text)", to get translations from JSON file provided by Futurenet
	 *
	 * @return Array
	 */
	public function updateAllTrans() {
		try {
			// update project languages in the plugin "storage" folder
			$aProjectLanguages = $this->manager->updateProjectLangs();
		} catch (Exception $e) {
			echo 'Exception: ', $e->getMessage(), "\n";
		}

		// update all view-files where "trans()" function with ternar operator and fnTrans()
		$aTransInViews = $this->manager->updateViewsTransTextWithJsonData();

		print_r($aTransInViews);
		return $aTransInViews;
	}

	/**
	 * Reaset all to previous state when used "updateAllTrans();"
	 *
	 * @return Array
	 */
	public function resetAllTrans() {
		$this->manager->resetViewsTrans();
		return ['status' => 'ok'];
	}

	/**
	 * Get project translations (will be used by fnTrans())
	 *
	 * @return JSON
	 * {
	{
	var_name : "cart_text",
	original_value : "Cart",
	value : "Корзина"
	},
	{
	var_name : "cart_text",
	original_value : "Cart",
	approved : 0,
	index : 20,
	approved_text : "Incorrect text"
	},
	{
	var_name : "cart_text",
	original_value : "Cart",
	approved : 1,
	index : 20,
	value : "Корзина"
	}
	 * }
	 */
	public function apiGetProjectTranslation() {
		$api = '/get/languages';
		$aProjectLanguages = self::apiGetProjectLangs();
		// foreach($aProjectLanguages as){

		// }
		$aPostVars = [
			'token' => $this->token,
			'project_id' => $this->project_id,
			'lang' => 'pl_PL',
		];

		//if ($aProjectLanguages) {
		print_r($aProjectLanguages);
		exit;
		//}

		// update project languages in the plugin "storage" folder
		$this->manager->updateProjectLangs();
	}

	/**
	 * Send project text for translations (later will be used by fnTrans())
	 *
	 * Send JSON
	 * {
	 *	{
	 *	  token : 11,
	 *	  lang : 'en',
	 *	  project_id : 11,
	 *	  translation: {
	 *	    {
	 *	        name: "cart_text",
	 *	        value: "Cart",
	 *	        approve: 0,
	 *	        status_text: "It its not correct translation"
	 *	    },
	 *	    {
	 *	        name: "cart_text",
	 *	        value: "Cart",
	 *	        approve: 1
	 *	    }
	 *	}
	 * }
	 */
	public function apiPutProjectTranslation($lang = '') {
		var_dump($lang);exit;
		if (empty($lang)) {
			return false;
		}

		$api = '/put/project/translation';
		$aProjectLanguages = $this->manager::getProjectLangs('short');
		// foreach($aProjectLanguages as){

		// }
		$aPostVars = [
			'token' => $this->token,
			'project_id' => $this->project_id,
			'lang' => 'pl_PL',
		];

		//if ($aProjectLanguages) {
		print_r($aProjectLanguages);
		exit;
		//}

		// update project languages in the plugin "storage" folder
		$this->manager->updateProjectLangs();
	}

	/**
	 * Get ids of all languages from Futurenet.
	 *
	 * @param $onlyCodes
	 * @return JSON
	 * stdClass Object(
	 *    [records] => Array
	 *        (
	 *            [0] => stdClass Object
	 *                (
	 *                    [id] => 3
	 *                    [name] => Українська
	 *                    [code] => ua_UA
	 *                )
	 *            [1] => stdClass Object
	 *	                (
	 *                    [id] => 4
	 *                    [name] => Русский
	 *                    [code] => ru_RU
	 *                )
	 *            ...
	 */
	public static function apiGetLanguages() {
		$api = '/get/languages';
		$token = parse_ini_file(app_path() . '/FnTranslation/.config')['TOKEN'];
		$futurenetDomain = parse_ini_file(app_path() . '/FnTranslation/.config')['FUTURENET_URL'];
		$aPostVars = ['token' => $token];
		return FnTranslationManager::httpRequest($futurenetDomain . $api, 'POST', $aPostVars);
	}

	/**
	 * Get project (user) languages
	 *
	 * @return array Ex.: [3=>"en_EN", 4=>"fr_FR"]
	 */
	public static function apiGetProjectLangs() {
		$api = '/get/project';

		// plugin credentials
		$token = parse_ini_file(app_path() . '/FnTranslation/.config')['TOKEN'];
		$project_id = parse_ini_file(app_path() . '/FnTranslation/.config')['PROJECT_ID'];
		$futurenetDomain = parse_ini_file(app_path() . '/FnTranslation/.config')['FUTURENET_URL'];

		$aPostVars = ['token' => $token, 'project_id' => $project_id];
		$jsonProjectLangs = FnTranslationManager::httpRequest($futurenetDomain . $api, 'POST', $aPostVars);

		$aProjectLanguages = json_decode($jsonProjectLangs);

		if ($aProjectLanguages->records->project_langs) {
			$aReturn = [];
			foreach ($aProjectLanguages->records->project_langs as $v) {
				$aReturn[$v->id] = $v->code;
			}
			return $aReturn;
		} else {
			return [
				'status' => 'error',
				'message' => 'Wrong response at apiGetProjectLangs();',
			];
		}

	}

	/**
	 * $aImportLangs Array of Translation for given land
	 * (file name corespons to short lang Ex. 'en.php')
	 *
	 * @param string - short lang
	 */
	public static function apiImportTranslationArray($lang = 'en') {
		$api = '/plugin_import_translation';
		$aPostVars = [
			'token' => $this->token,
			'project' => $this->project_id,
		];

		$jsonImportLangs = $this->manager::httpRequest($this->futurenetDomain . $api, 'POST', $aPostVars);

		$aImportLangs = json_decode($jsonImportLangs);
		// !!! - $aImportLangs must be PHP ARRAY
		if ($aImportLangs) {
			// add new translations in files
			$this->manager->saveTranslationInStorage($lang, $aImportLangs);

			return [
				'status' => 'ok',
				'$aImportLangs size' => sizeof($aImportLangs),
			];
		} else {
			return [
				'status' => 'error',
				'message' => "'lang_structure' - is empty or not exists",
			];
		}
	}
}
