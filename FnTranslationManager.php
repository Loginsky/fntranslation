<?php namespace App\FnTranslation;

use App\FnTranslation\Controllers\FnTranslationController;
use App\FnTranslation\FnTranslationBase;
use Illuminate\Events\Dispatcher;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Lang;
use Symfony\Component\Finder\Finder;

/**
 * Class FnTranslationManager - responsible for plugin logic
 */

class FnTranslationManager extends FnTranslationBase {

	protected $app;
	protected $files;
	protected $events;

	protected $project_id;
	protected $token;
	protected $ip;
	protected $default_lang;

	public function __construct(Application $app, Filesystem $files, Dispatcher $events) {
		$this->app = $app;
		$this->files = $files;
		$this->events = $events;

		$this->project_id = parse_ini_file(app_path() . '/FnTranslation/.config')['PROJECT_ID'];
		$this->token = parse_ini_file(app_path() . '/FnTranslation/.config')['TOKEN'];
		$this->ip = parse_ini_file(app_path() . '/FnTranslation/.config')['FUTURENET_IP'];
		$this->default_lang = parse_ini_file(app_path() . '/FnTranslation/.config')['DEFAULT_LANG'];
		$this->futurenetDomain = parse_ini_file(app_path() . '/FnTranslation/.config')['FUTURENET_URL'];
	}

	/**
	 * Logic to Export langs from App to Futurenet
	 *
	 * @return $aExport Array - $aExport[] = array(
	 *								'var_name' => '',
	 *								'desciprtion' => '',
	 *								'project_id' => 1,
	 *							);
	 */
	public function exportLangs() {
		$aExport = [];

		foreach ($this->files->directories(base_path() . '/resources/lang') as $langPath) {
			$locale = basename($langPath);

			foreach ($this->files->allfiles($langPath) as $file) {

				$info = pathinfo($file);
				$group = $info['filename'];

				if (in_array($group, array())) {
					continue;
				}

				$subLangPath = str_replace($langPath . DIRECTORY_SEPARATOR, "", $info['dirname']);
				if ($subLangPath != $langPath) {
					$group = $subLangPath . "/" . $group;
				}

				$langs = \Lang::getLoader()->load($locale, $group);
				if ($langs && is_array($langs)) {
					foreach (array_dot($langs) as $key => $value) {
						// process only string values
						if (is_array($value)) {
							continue;
						}
						// NOTE: "trans()" use $group.'.'.$key inside, trans($group.'.'.$key)
						$aExport[] = array(
							'var_name' => $locale . '/' . $group . '.' . $key,
							'desciprtion' => (string) $value,
							'project_id' => $this->project_id,
						);
					}
				}
			}
		}
		return $aExport;
	}

	/**
	 * Logic for collected langs from Futurenet API and "resources/lang/" files
	 *
	 * @param $aJson array
	 * @return int (number of files saved)
	 */
	public function importLangs($aJson) {
		$tree = $this->makeTree($aJson);
		$pathLang = base_path() . '/resources/lang';

		// create copy "../lang_date"
		$this->files->copyDirectory($pathLang, $pathLang . '_' . date("Y-m-d_H-i-s", time()));

		// create new "../lang" imported from Futurenet
		if ($tree) {
			$countFiles = 0;
			foreach ($tree as $locale => $groups) {
				$kName = key($groups);
				foreach ($groups as $group) {
					foreach ($group as $langs) {
						$path = $pathLang . '/' . $locale . '/' . $kName . '.php';
						$output = "<?php\n\nreturn " . var_export($group, true) . ";\n";
						$this->files->put($path, $output);
						$countFiles++;
					}
				}
			}
			return $countFiles;
		} else {
			return 0;
		}

	}

	/**
	 * Make tree to import JSON with translations to website
	 *
	 *  @param $langs array
	 *  @return $array array
	 */
	protected function makeTree($langs) {
		// get languages codes
		$aLanguageCodes = self::getProjectLangs('short');
		// make tree
		$array = array();
		if ($langs) {
			foreach ($langs->lang_structure as $lang) {
				// "en/admin/admin.php" to "en/admin/admin"
				if (strpos($lang->path, 'php')) {
					$group = substr(substr($lang->path, 3), 0, -4);
				} else {
					$group = $lang->path;
				}
				// en (ISO 2 "Letter Language Codes")
				$lacal = substr($lang->path, 0, 2);
				// set structure
				if (in_array($lacal, $aLanguageCodes)) {
					array_set(
						$array[$lacal][$group],
						$lang->var,
						$lang->value
					);
				} else {
					print 'Error: "path" not right (Language not matching its code) at <i>makeTree();</i> is - <strong>"' . $lang->path . '"</strong>, but should be - Ex.: <strong>"en/admin/admin.php"</strong>';
					exit;
				}

			}
		}

		return $array;
	}

	/**
	 * Get all vars/keys in "trans(string/$variable)" for translation
	 *
	 * If site never used multi-language, site administrator can include all
	 * text (not PHP/HTML.. code) in "../resources/views/.." files in "trans()" function, then Futurenet can adopt it for translations
	 *
	 *  @param $path string
	 *  @return $aVars - Array([0] => [
	 *          'path' => resources\views\admin\contentjobcategories\create_edit.blade.php
	 *       	'var' => "admin/modal.name"
	 *      	'function' => trans("admin/modal.general")
	 *      ]);
	 */
	public function findTransFunctions() {
		$aVars = array();
		$path = base_path() . '/resources/views';

		$functions = array('trans\((.*?)\)', 'trans_choice\((.*?)\)', 'Lang::get\((.*?)\)', 'Lang::choice\((.*?)\)', 'Lang::trans\((.*?)\)', 'Lang::transChoice\((.*?)\)', '@lang\((.*?)\)', '@choice\((.*?)\)', '__\((.*?)\)');
		$pattern = "(" . implode('|', $functions) . ")";

		// Find all PHP files in path
		$finder = new Finder();
		$finder->in($path)->name('*.php')->files();

		/** @var \Symfony\Component\Finder\SplFileInfo $file */
		if ($finder) {
			foreach ($finder as $fKey => $file) {
				// Search the current file for the pattern
				if (preg_match_all("/$pattern/i", $file->getContents(), $matches)) {

					// Get trans() use
					foreach ($matches[2] as $k => $key) {
						$aVars[] = [
							'path' => $file->getRelativePathname(),
							'var' => $key,
							'function' => $matches[0][$k],
						];
					}

				}
			}
		}

		// Return the number of found translations
		return $aVars;
	}

	/**
	 * Check request token to allow request via plugin
	 *
	 * @param Request $request
	 * @return (bool)
	 */
	public function updateProjectLangs() {
		$aProjectLangs = FnTranslationController::apiGetProjectLangs();
		$output = "<?php\n\nreturn " . var_export($aProjectLangs, true) . ";\n";
		if ($aProjectLangs && $this->files->put(app_path() . '/FnTranslation/storage/project-langs', $output)) {
			return $aProjectLangs;
		} else {
			return NULL;
		}
	}

	/**
	 * Update all view-files where "trans()" function with ternar operator and fnTrans()
	 * like :
	 * ..>(Session::get('locale')=='en')?$modalGeneral:fnTrans($modalGeneral)</..
	 * and save copy of old ones like:
	 *
	 * @return (bool)
	 */
	public function updateViewsTransTextWithJsonData($method = 'variable') {
		$pathResources = base_path() . '/resources';
		$viewsPath = $pathResources . '/views';
		$copyExists = false;
		// 1. get default lang code
		$defaultLangCode = $this->getDefaultLangCode('short');
		// 2. check if there is a copy of views, get to prev (start) state
		$this->resetViewsTrans();
		// 3. make copy of all views
		$this->files->copyDirectory($viewsPath, $viewsPath . '_' . date("Y-m-d_h-i", time()));
		// 4. Find all trans() functions in view files
		$aTransInViews = $this->findTransFunctions();
		// 5. Replace original "trans()" to "fnTrans()"
		if ($aTransInViews) {
			foreach ($aTransInViews as $v) {
				$viewFile = $viewsPath . '/' . $v['path'];
				$var = ($method == 'variable') ? $v['var'] : $v['function'];
				$replaceFunction = "(Session::get('locale')=='" . $defaultLangCode . "')?" . $var . ':fnTrans(' . $v['var'] . ')';
				//read file content
				$fileContent = $this->files->get($viewFile);
				//replace
				$fileContent = str_replace($v['function'], $replaceFunction, $fileContent);
				//write all content
				$this->files->put($viewFile, $fileContent);
			}
			// end
		}
		return $aTransInViews;
	}

	/**
	 * Bring to prev state all views when used "updateViewsTransTextWithJsonData();"
	 * @return (bool)
	 */
	public function resetViewsTrans() {
		// check if there is a copy of views, get it back in place
		$pathResources = base_path() . '/resources';
		$viewsPath = base_path() . '/resources/views';
		foreach ($this->files->directories($pathResources) as $dirPath) {
			$aViewPath = explode('resources', $dirPath);
			// get copy of old views
			if (strpos($aViewPath[1], 'views_') !== false) {
				// delete new views
				$this->files->cleanDirectory($viewsPath);
				$this->files->deleteDirectory($viewsPath);
				// old copy back to original state
				$this->files->copyDirectory($dirPath, $viewsPath);
				// remove old copy
				$this->files->cleanDirectory($dirPath);
				$this->files->deleteDirectory($dirPath);
			}
		}

	}

	/**
	 * php $aTranslations Array files (file name corespons to short lang Ex. 'en.php')
	 * @param string - short lang
	 * @param Array - $aTranslations
	 */
	public function saveTranslationInStorage($langShortCode = 'en', $aTranslations = []) {
		$myFile = app_path() . '/FnTranslation/storage/' . $langShortCode . '.php';
		$fileContent = '';
		$aFileTranslations = [];
		if ($this->files->isFile($myFile)) {
			$aFileTranslations = @include_once app_path() . '/FnTranslation/storage/' . $langShortCode . '.php';
		}

		$fileContent = array_merge((array) $aFileTranslations, $aTranslations);
		$output = "<?php\n\nreturn " . var_export($fileContent, true) . ";\n";
		//write all content
		$this->files->put($myFile, $output);
	}

	/**
	 * Get default language code
	 * @param string - define lang code length
	 * @return (string)
	 */
	public function getDefaultLangCode($langCodeLangth = 'long') {
		$aProjectLangs = self::getProjectLangs();
		if ($aProjectLangs) {
			$lang = isset($aProjectLangs[$this->default_lang]) ? $aProjectLangs[$this->default_lang] : 'en';
		} else {
			$lang = 'en_EN';
		}
		return ($langCodeLangth == 'short') ? substr($lang, 0, 2) : $lang;
	}

	/**
	 * Check request token to allow request via plugin
	 *
	 * @param Request $request
	 * @return (bool)
	 */
	public function checkRequestIP(Request $request) {
		return $request->ip() == $this->ip;
	}

	/**
	 * Get site language from URL
	 *
	 * @param Request
	 * @return (string) "en"
	 */
	public function getLangFromUrlQuery($request) {
		// get default languages
		$aProjectLangs = self::getProjectLangs('short');
		if ($aProjectLangs && is_array($aProjectLangs)) {
			if ($request->query()) {
				foreach ($request->query() as $lang) {
					if (in_array($lang, $aProjectLangs)) {
						return $lang;
					}
				}
			}

		}
	}

}