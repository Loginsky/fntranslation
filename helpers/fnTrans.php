<?php
use App\FnTranslation\FnTranslationBase;
// Will use Lang Array in "storage" file (provided by Futurenet) for translations
// if translation not found, sending the text (as a key for other language) to:
// 		> api.fnukraine.pp.ua/put/project/translation
// .. for future translations
function fnTrans($var) {
	$aLang = @include_once app_path() . '/FnTranslation/storage/' . Session::get('locale') . '.php';
	if (isset($aLang[$var])) {
		return $aLang[$var];
	} else {
		$aFuturenetTranslations[]['name'] = $var;

		// TODO: need to be called at the end once
		$FnTranslationBase = new FnTranslationBase();
		$FnTranslationBase::sendNewFnTrans($aFuturenetTranslations);

		return $var;
	}
}