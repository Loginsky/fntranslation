<?php
Route::group(array('namespace' => 'App\FnTranslation\Controllers'), function () {
	// url pattern
	Route::pattern('slug', '[0-9a-z-_]+');

	// Plugin Home
	Route::get('fntranslation', 'FnTranslationController@index');

	/******************** Export/Import Laravel Lang files ************************/
	// Export company-site default language
	Route::get('futurenet-export-langs', 'FnTranslationController@pluginExportLangs');
	// Import ready translations
	Route::get('futurenet-import-langs', 'FnTranslationController@pluginImportLangs');

	/************************* To work with "fnTrans()" ***************************/
	// Get all trans(string) keys/vars in view files
	Route::get('fntranslation/get-all-trans', 'FnTranslationController@getAllTrans');
	// Update all view files where "trans(text/key/$text)" become in ternary use
	Route::get('fntranslation/update-all-trans', 'FnTranslationController@updateAllTrans');
	// Bring to prev (start) state all views when used "fntranslation/update-all-trans"
	Route::get('fntranslation/reset-all-trans', 'FnTranslationController@resetAllTrans');
	// Get project translations as arrays
	Route::get('fntranslation/get-project-trans', 'FnTranslationController@apiGetProjectTranslation');
	// Send new text for translation
	Route::get('fntranslation/put-project-trans/{lang}', 'FnTranslationController@apiPutProjectTranslation');
	// Import Translation Array for Lang (Ex.: en) php Array
	Route::get('fntranslation/import-project-trans/{lang}', 'FnTranslationController@apiImportTranslationArray');

	/**************** Change site global language (could be example) ****************/
	Route::get('fntranslation/language/{lang}', 'FnTranslationController@language');
	Route::get('fntranslation/language', 'FnTranslationController@language');

	/****************************** Helpers ***************************************/
	// get all ids of languages
	Route::get('fntranslation/available-langs', 'FnTranslationController@apiGetLanguages');
	// get all languages of project
	Route::get('fntranslation/project-langs', 'FnTranslationController@apiGetProjectLangs');

	/**************************** Used for PhpUnits ********************************/
	Route::get('fntranslation/export-array', 'FnTranslationController@exportLangsArray');
	Route::get('fntranslation/import-array', 'FnTranslationController@importLangsArray');

});
